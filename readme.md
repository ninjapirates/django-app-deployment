# Django App Deployment

## Documentation

[docs/](./docs/readme.md)

## Sample Configuration Files

[samples/](./samples/readme.md)

## Ideas for future improvements

- In general, a way to _push_ app updates, instead of having to log into the server and pull them from bitbucket
- We can't get to the server from off campus since ITS cracked down on the firewall for einstein. Ask Mr. Nielsen if anything has changed since last semester - do we have any new options?
- Can we use [docker][docker] to simplify things?
- If we can't/won't use docker, what about something like [puppet][puppet] for managing the server configuration declaratively?
- Bitbucket has a [built-in CI service][bitbucket-ci] - can we trigger deployments automatically?
- What about [jenkins][jenkins]?
- Instead of using on-campus resources, maybe look into something like [Microsoft Azure][azure-student] or Google Cloud Platform or Amazon Web Services; many of these services have student offers that might make sense for us. Then we would have full control of our infrastructure and get to practice with modern cloud tools.

[docker]: https://www.docker.com/why-docker
[puppet]: https://puppet.com/solutions/build-and-deploy-applications
[bitbucket-ci]: https://bitbucket.org/product/features/pipelines
[jenkins]: https://jenkins.io/
[azure-student]: https://azure.microsoft.com/en-us/free/students/

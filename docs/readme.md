# Django App Deployment

Documentation and scripts for deploying our Django apps on our department test server, `cs5910`.

## Server Package Dependencies

See [./server_prereqs](./server_prereqs.md)

## Setting up a new app

See [./new_app_setup](./new_app_setup.md)

## Updating an existing app

See [./deploying_updates](./deploying_updates.md)

## Retiring an app

To-do

Basically, undoing the things in "Setting up a new app" to make room for a new app

## Server Maintenance

To-do

We don't have an automatic backup solution - it would be nice to be able to do scheduled or on-demond backups.

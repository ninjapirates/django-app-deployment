# Server Prerequisites

Packages that need to be installed on the deployment server.

Assumptions:
- OS is Debian-derivative Linux (e.g. Ubuntu)

## `nginx`

> "[nginx](https://nginx.org/en/) [engine x] is an HTTP and reverse proxy server, a mail proxy server, and a generic TCP/UDP proxy server"

We use nginx as our static file server. It forwards requests to a Python WSGI application server (gunicorn) which runs the Django app.

## `postgresql`

> "[PostgreSQL]((https://www.postgresql.org/)), often simply Postgres, is an object-relational database management system (ORDBMS) with an emphasis on extensibility and standards compliance."

Postgres is What we use to back Django's ORM features.

## `python3`

Some Linux distributions don't come with Python 3 by default, only Python 2.7 - we use Python 3.x for all our Django apps.

## `supervisor`

> "[Supervisor](http://supervisord.org/introduction.html) is a client/server system that allows its users to control a number of processes on UNIX-like operating systems"

We use supervisor to control our gunicorn server processes.

Supervisor can be installed either via [pip](http://supervisord.org/installing.html#installing-via-pip) (using the global `pip3`, *not* inside a virtual environment!) or from a [disitribution package](http://supervisord.org/installing.html#installing-a-distribution-package) (using `apt`).

## `git`

> "[Git](https://git-scm.com/) is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency."

Since we use git for source code change management, it is easiest to copy the app code to the server by cloning the remote repository (e.g. Bitbucket).

A relatively recent version of Git is usually pre-installed on Ubuntu server.

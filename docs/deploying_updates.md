# Deploying an update

This is a very brittle and manual way of deploying updates, so definitely a good area to try to improve!

## Stop the app server

Stop the gunicorn server using supervisor:

```
# supervisorctl stop <app-name>
```

This stops the python server; but nginx will still be running. You can instead stop nginx if you want, but this way at least the site will keep returning 404 responses.

## Change to the user of the app

After logging in to the server, you can use `sudo -i -u ...` to switch to the application user. For example, for the health data system, the user is `hds-app`, so the command is

```
$ sudo -i -u hds-app
```

This should drop you in the home folder for that user, e.g. `/home/hds-app/`. You will want to activate the python virtual environment for the project:

```
$ source bin/activate
```
## Update the application

### Get the new version from Bitbucket

Navigate into the `code/` folder and use git to retrieve the version to be deployed from Bitbucket. This will usually be the `master` branch, or a [tag](https://git-scm.com/book/en/v2/Git-Basics-Tagging) for a specific version.

If you're just getting the latest commits on the master branch, it should be as simple as

```
$ git checkout master
$ git pull
```

Now the latest version from the master branch is on the server.

### Migrate static files

The static assets the app uses need to be moved from their location in `code/static/` to the directory nginx serves static files from. Django has an admin command for this, assuming `settings.py` is configured correctly to match however nginx is configured.

```
$ python manage.py collectstatic
```

[Django Admin collectstatic docs](https://docs.djangoproject.com/en/2.1/ref/contrib/staticfiles/#django-admin-collectstatic)

### Apply database migrations

If there have been model changes since the last update, database migrations will need to be applied

```
$ python manage.py migrate
```

[Django Admin migrate docs](https://docs.djangoproject.com/en/2.1/ref/django-admin/#migrate)

(Cross your fingers that you don't have to dive into Postgres and fix anything manually.)

## Restart the app server

Change back to your regular user account; if you used `sudo -i -u` to change earlier, you can just use `exit` to be logged out of the app user and be back at your regular user prompt.

Then do:
```
# supervisorctl start <app-name>
```

You can check if it is running using
```
# supervisorctl status <app-name>
```


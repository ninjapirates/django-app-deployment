# Setting up a new app for deployment

This describes the process of setting up a new application on our Ubuntu Linux test server.

## Notes

- There will be several secure strings (passwords, tokens, etc.) created for a new application. It's recommended to use a password manager to record these.
- When showing shell commands below, `#` is used to prefix commands that need to be executed with superuser privileges (i.e. with `sudo`); other shell commands are prefixed with `$`.
- Much (but not all!) of this info is sourced from this guide: https://simpleisbetterthancomplex.com/tutorial/2016/10/14/how-to-deploy-to-digital-ocean.html

## 0 Linux Account

Create a user and group for the application. This greatly simplifies permissions and allows us to use the new user's home directory to contain the application's virtual environment, python code, and static files.

Name: application abbreviation + "app", e.g.

"Simple File Share" has user "sfs-app", "Health Data System" has "hds-app".

You can use the following interactive command, replacing `app-name` with an actual name in the form described above:
```
# adduser app-name
```

This command will create a group `app-name` and a user `app-name` that is a member of that group, and populates a home directory `/home/app-name`. You can then change to this user account with `$ sudo -i -u app-name`.

Provide a generated password and a descriptive Full Name when prompted; you can leave the blank defaults for the rest of the information.

## 1 Database Setup

The every application should have it's own database user for connecting to the Postgres server.

First, change users to the postgres account:

```
$ sudo -i -u postgres
```

Then enter the postgres interactive prompt with

```
$ psql
```

In SQL, you can't use '-' in identifiers, so instead of "app-name" we use "app_name" (an underscore instead of a hyphen).
Issue the following SQL commands, replacing `app_name` with the _same name as the user account created above_ (only with an underscore).

```
CREATE DATABASE app_name_db;
CREATE ROLE app_name WITH LOGIN PASSWORD '<generate-a-password>';
GRANT ALL PRIVILEGES ON DATABASE app_name_db TO app_name;
ALTER ROLE app_name SET client_encoding TO 'utf8';
ALTER ROLE app_name SET default_transaction_isolation TO 'read committed';
ALTER ROLE app_name SET timezone TO 'UTC';
```

The first three commands create a database for the application to use, create a user for the application, and grant permissions for the database to the new user. The next three modify the settings for the user to match the required settings for django apps (see Django's [PostgreSQL Notes](https://docs.djangoproject.com/en/2.0/ref/databases/#optimizing-postgresql-s-configuration)).

## 2 Setting a deployment key and cloning the project code

In the Bitbucket repository for the application, go to 'Settings > General > Access Keys' (you need 'Administrator' privileges on the repository for this). Follow [these instructions](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html#SetupanSSHkey-ssh2) to generate a key _as the app user_, then follow [these instructions](https://confluence.atlassian.com/bitbucket/use-access-keys-294486051.html) to add the key to the repository. This will grant the app user read (but not write) priviliges to the code repository, so it can clone the project.

- When generating a key, the defaults are all acceptable and setting a password for the key is _not_ required.
- When copying the key to Bitbucket, be sure to copy the _public_ key (file ends in .pub); the private key should not leave the server!

Quick summary:

1. If you are not already the app user: `$ sudo -i -u app-name`
2. Generate the key pair: `$ ssh-keygen` (accept all the defaults)
3. Output the public key: `$ cat ~/.ssh/id_rsa.pub` (select and copy the text)
4. On the Bitbucket repo, go to Settings > Access Keys > Add key
5. Paste the key text into the field, set a name, and save the key

Now you should be able to clone the repository now; do this when in the app users home directory:

```
$ cd ~ && git clone <repo-ssh-url-here> code
```

This will create a directory called "code" in the app user's home directory and download the contents of the repository's default branch (usually `develop`). Because the access key only allows read access, you can do any regular git operation inside the "code" folder (e.g. fetch any branch), but cannot push any changes back to Bitbucket.

## 3 Setup python virtual environment

In the new app user's home directory, we want to create a new python virtual environment for the application to use. _Do not use virtualenvwrapper for this._ virtualenvwrapper abstracts away the location of the virtual environment's files - in this case we want more control so we can reference executables in the virtual environment.

Change to the app user, then create a new virtual environment in the current directory (which should be the app user's home directory):
```
$ sudo -i -u app_name
$ virtualenv .
```

The command should have output like the following, and add bin, include, and lib folders to the home directory:
```
hds-app@cs5910:~$ virtualenv .
Using base prefix '/usr'
New python executable in /home/hds-app/bin/python3
Also creating executable in /home/hds-app/bin/python
Installing setuptools, pip, wheel...done.
hds-app@cs5910:~$ ls
bin  code  include  lib  pip-selfcheck.json
```

Activate the virtual environment:
```
$ source ./bin/activate
```

Use pip to install the project's python module dependencies:
```
$ cd code
$ pip install -r requirements.txt
```

If a `requirements.txt` is not setup yet, you will need to `pip install` at least `Django`, `gunicorn`, and `psycopg2`.


## 4 Configuring gunicorn and supervisor

Gunicorn is the application server that runs the django app. We manage gunicorn as a service using an application called Supervisor.

First, create a script `bin/gunicorn_start`; you'll need to change the variables to match the directories, users, applications, and django module names for the actual application. I have user `<APP-NAME>` and `<PROJECT-MODULE>` as placeholders.

```
#!/bin/bash

NAME="name of the app"
DIR=/home/<APP-NAME>/code
USER=<APP-NAME>
GROUP=<APP-NAME>
WORKERS=2
BIND=unix:/home/APP-NAME/run/gunicorn.sock
DJANGO_SETTINGS_MODULE=<PROJECT-MODULE>.settings
DJANGO_WSGI_MODULE=<PROJECT-MODULE>.wsgi
LOG_LEVEL=error

cd $DIR
source ../bin/activate

export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DIR:$PYTHONPATH

exec ../bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $WORKERS \
  --user=$USER \
  --group=$GROUP \
  --bind=$BIND \
  --log-level=$LOG_LEVEL \
  --log-file=-

```

Make it executable, and make directories for the socket and logs to live in:
```
$ chmod u+x bin/gunicorn_start
$ mkdir run
$ mkdir logs
$ touch logs/gunicorn-error.log
```

Create a supervisor configuration file called `/etc/supervisor/conf.d/APP-NAME.conf` with the following contents:

```
[program:APP-NAME]
command=/home/APP-NAME/bin/gunicorn_start
user=APP-NAME
autostart=true
autorestart=true
redirect_stderr=true
stdout_logfile=/home/APP-NAME/logs/gunicorn-error.log
```

Of course, replace "APP-NAME" with the names of the user and/or application as needed; the name at the top of the config file (in the `[program:]` part) is what will be used to actually start and stop the server, so that can be tweaked if desired.

Have supervisor read and load the new config file with

```
# supervisorctl reread
# supervisorctl update
```

The service can then be controlled with

```
sudo supervisorctl [status|start|stop|restart] APP-NAME
```

## 5 Configuring Nginx

Create a new nginx site configuration file called `/etc/nginx/sites-available/APP-NAME`.

```
upstream app_server {
    server unix:/home/APP-NAME/run/gunicorn.sock fail_timeout=0;
}

server {
    listen 80;

    # add here the ip address of your server
    # or a domain pointing to that ip (like example.com or www.example.com)
    server_name 151.141.91.27;

    keepalive_timeout 5;
    client_max_body_size 4G;

    access_log /home/APP-NAME/logs/nginx-access.log;
    error_log /home/APP-NAME/logs/nginx-error.log;

    location /static/ {
        alias /home/APP-NAME/static/;
    }

    # checks for static file, if not found proxy to app
    location / {
        try_files $uri @proxy_to_app;
    }

    location @proxy_to_app {
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $http_host;
      proxy_redirect off;
      proxy_pass http://app_server;
    }
}
```

Create a symlink to the file in the "sites-enabled" directory (this is what tells nginx to activate the configuration). 

```
# ln -s /etc/nginx/sites-available/APP-NAME /etc/nginx/sites-enabled/APP-NAME
```

_Be aware that if there was another Django app running, the new configuration may conflict with the old one._ A configuration supporting multiple apps at once is to-do; for now just make sure to remove the symlink for any retired apps.

Be sure to remove the nginx default site if it has not been already:

```
# rm /etc/nginx/sites-enabled/default
```

Restart the nginx service:

```
# service nginx restart
```

## 6 `settings.py` and secret keys

There are several areas of the `settings.py` for a project that need to be configured correctly to match the test server environment.

### Determining the environment

Although not flexible nor entirely robust, I have been checking the hostname to switch inside the configuration file when settings need to differ between development machines and production. A better solution to investigate may be to split settings.py into multiple files, and include the production file conditionally.

```
import os
import socket

# Turn this to true, and the app will use Debug/Dev settings, even in production.
FORCE_DEBUG = False

HOSTNAME = socket.gethostname()
ON_CS5910 = HOSTNAME == 'cs5910'

# We will run in debug mode if we're not on the cs5910 server,
# or if we have FORCE_DEBUG turned on.
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = (not ON_CS5910) or FORCE_DEBUG
```

### Secret Key

Example for getting the secret key out of a file called "secret_key.txt" in the project base directory:

```
def get_secret_key():
	if (ON_CS5910):
		sk_path = os.path.join(BASE_DIR, 'secret_key.txt')
		with open(sk_path) as f:
			return f.read().strip()

	print("Using debug key")
	return '57rong 5ecre7 Key 7hi5 I5 No7, For Debug I7 15.'


SECRET_KEY = get_secret_key()
```

_"secret_key.txt" must be excluded from source control._

### Allowed hosts

This setting is required in production, but not in debug/development mode, so we can set it conditionally as follows:

```
def get_allowed_hosts():
	if (ON_CS5910):
		# including localhost so we can use SSH tunneling...
		# might want to clean that out later
		return ['localhost', '151.141.91.27']
	else:
		# when developing, let Django validate with their defaults
		return []


ALLOWED_HOSTS = get_allowed_hosts()
```

### Database configuration

So far, we have been able to use SQLite on developer machines, while switching to MariaDB or PostgreSQL in deployment. We conditionally return a different configuration as follows:

#### MySQL / MariaDB

```
def get_database_config():
	if (ON_CS5910):
		db_pw_path = os.path.join(BASE_DIR, 'mydbpw.txt')
		with open(db_pw_path) as f:
			dbpw = f.read().strip()
			return {
				'ENGINE': 'django.db.backends.mysql',
				'NAME': 'django_sfs',
				'USER': 'django_sfs_user',
				'PASSWORD': dbpw,
				'HOST': 'localhost',
				'PORT': '3306',
			}
	else:
		return {
			'ENGINE': 'django.db.backends.sqlite3',
			'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
		}


DATABASES = {
	'default': get_database_config(),
}
```

#### PostgreSQL

```
def get_database_config():
	if (ON_CS5910):
		db_pw_path = os.path.join(BASE_DIR, 'mydbpw.txt')
		with open(db_pw_path) as f:
			dbpw = f.read().strip()
			return {
				'ENGINE': 'django.db.backends.postgresql',
				'NAME': 'DATABASE_DB_NAME',
				'USER': 'DATABASE_USER_NAME',
				'PASSWORD': dbpw,
				'HOST': 'localhost',
				'PORT': '5432',
			}
	else:
		return {
			'ENGINE': 'django.db.backends.sqlite3',
			'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
		}


DATABASES = {
	'default': get_database_config(),
}
```

The password should be stored in a a text file containing only the password (in this example called 'mydbpw.txt'), stored in the same directory as the secret key file. _The database password must be excluded from source control._

### Static files configuration

```
# when referencing a static file, the URL is the hostname + this path +
# the file path relative to whatever directory the static files are actually in
STATIC_URL = '/static/'

# By default, django.contrib.staticfiles will look for files in
# "<project>/<app>/static"; this adds an additional directory to search,
# so we can also put files in "<project>/static/" if the file is used by
# several different apps
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]

# this is the folder that files will be put in when we run
# "python manage.py collectstatic". Either the files need to be served directly out of this folder,
# or they then need to be moved out of that folder and into a location where
# our static file solution can serve them
STATIC_ROOT = '/home/APP-NAME/static/'
```

### (Optional) Media files configuration

```
# serving "/media/" (e.g. user uploads)
# static files that are not wen assets (e.g. user uploads)
# will be served from this URL; the web server (e.g. nginx)
# should be configured to serve requests to this URL from
# the MEDIA_ROOT directory specified below
MEDIA_URL = '/media/'

# when deployed, the media directory will be in the app user's home
# directory (just like static/); when using the django development
# server, files are uploaded to and served from a 'media' folder in
# the project root (so add that to .gitignore!)
MEDIA_ROOT = '/home/APP-NAME/media/' if ON_CS5910 else os.path.join(BASE_DIR, 'media')
```

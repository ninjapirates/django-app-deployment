# Sample configuration files

For an app referred to as "hds-app":

## nginx site

[/etc/nginx/sites-available/hds-app](./nginx_site)

## supervisor process

[/etc/supervisor/conf.d/hds-app.conf](./supervisor_conf)

## gunicorn starter

[/home/hds-app/bin/gunicorn_start](./gunicorn_start)